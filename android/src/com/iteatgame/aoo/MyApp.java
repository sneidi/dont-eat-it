package com.iteatgame.aoo;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.onesignal.OneSignal;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import java.util.Map;

public class MyApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        initAppsFlyer();
        initOneSignal();
        initAppMetrica();
    }


    private void initOneSignal(){
        String oneSignalKey = getString(R.string.api_onesignal);
        if(oneSignalKey==null || oneSignalKey.length()<2) return;



        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(oneSignalKey);

    }

    private void initAppsFlyer(){
        String AppsfKey = getString(R.string.api_appsflyer);
        if(AppsfKey==null || AppsfKey.length()<2) return;

        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {
            @Override
            public void onConversionDataSuccess(Map<String, Object> conversionData) {

                for (String attrName : conversionData.keySet()) {
                }
            }

            @Override
            public void onConversionDataFail(String errorMessage) {
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> attributionData) {
                for (String attrName : attributionData.keySet()) {
                }
            }

            @Override
            public void onAttributionFailure(String errorMessage) {
            }
        };

        AppsFlyerLib.getInstance().init(AppsfKey, conversionListener, this);
        AppsFlyerLib.getInstance().start(this);

    }

    private void initAppMetrica(){
        String appMetricaKey = getString(R.string.api_appmetrica);
        if(appMetricaKey==null || appMetricaKey.length()<2) return;

        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder(appMetricaKey).build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
