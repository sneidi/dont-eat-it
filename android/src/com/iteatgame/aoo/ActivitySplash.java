package com.iteatgame.aoo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowInsets;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import im.delight.android.webview.AdvancedWebView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.SYSTEM_UI_FLAG_VISIBLE;

public class ActivitySplash extends AppCompatActivity {

    private View progressView;
    private AdvancedWebView viewView;
    private ViewViewController controller;


    public AdvancedWebView getViewView() {
        return viewView;
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressView = findViewById(R.id.progress_view);
        viewView = findViewById(R.id.view_view);
        controller = new ViewViewController(this);
        api();
    }

    private void api() {
        showProgress();
        String apiUrl = getString(R.string.api_url);
        if (apiUrl == null || apiUrl.length() < 2) {
            ga();
            return;
        }

        ApiClient.build(this).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful() && response.body() != null && response.body().startsWith("http")) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        getWindow().getDecorView().getWindowInsetsController().show(
                                WindowInsets.Type.statusBars()
                        );
                    } else {
                        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_VISIBLE);
                    }

                    controller.show(response.body());
                } else ga();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                ga();
            }
        });

    }

    public void showContent() {

        viewView.setVisibility(View.VISIBLE);
        progressView.setVisibility(View.GONE);
    }

    public void showProgress() {
        viewView.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);
    }

    public void showError() {
        Toast.makeText(this, getString(R.string.error_view), Toast.LENGTH_LONG).show();
        ga();
    }

    private void ga() {
        startActivity(new Intent(this, ActivityGame.class));
        finish();
        overridePendingTransition(0, 0);
    }

    public void exit() {
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        controller.openFile(requestCode, resultCode, intent);
    }

    @Override
    public void onBackPressed() {
        controller.back();
    }
}
