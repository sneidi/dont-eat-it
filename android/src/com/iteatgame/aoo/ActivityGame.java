package com.iteatgame.aoo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.gamedecst.AndroidInterface;

public class ActivityGame extends AndroidApplication implements AndroidInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useAccelerometer = false;
        config.useCompass = false;
        config.hideStatusBar = false;
        initialize(new MyGame(this), config);
    }

    @Override
    public void toast(String t) {
        handler.post(() -> Toast.makeText(ActivityGame.this, t, Toast.LENGTH_LONG).show());

    }

    @Override
    public void openPolicy() {
        startActivity(new Intent(this,ActivityPolicy.class));
    }
}
