package com.iteatgame.aoo;

import android.content.Context;
import android.net.Uri;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class ApiClient {


    public static Call<String> build(Context ctx) {
        String url = ctx.getString(R.string.api_url);
        Uri uri = Uri.parse(url);
        String baseUrl = uri.getScheme() + "://" + uri.getHost() + "/";

        String UA = System.getProperty("http.agent");  // Get android user agent.

        OkHttpClient okHttp = new OkHttpClient().newBuilder().addInterceptor(new UserAgentInterceptor(UA)).build();

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(ScalarsConverterFactory.create());
        Retrofit retrofit = builder.client(okHttp).build();
        return retrofit.create(ApiApi.class).auth(url);

    }

    public static class UserAgentInterceptor implements Interceptor {

        private final String userAgent;

        public UserAgentInterceptor(String userAgent) {
            this.userAgent = userAgent;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request requestWithUserAgent = originalRequest.newBuilder()
                    .header("User-Agent", userAgent)
                    .build();
            return chain.proceed(requestWithUserAgent);
        }
    }

    public interface ApiApi {
        @GET()
        Call<String> auth(@Url String url);
    }
}
