package com.iteatgame.aoo;

import android.content.Intent;
import android.graphics.Bitmap;

import im.delight.android.webview.AdvancedWebView;

public class ViewViewController implements AdvancedWebView.Listener {

    private ActivitySplash splash;

    public ViewViewController(ActivitySplash splash) {
        this.splash = splash;
        initView(splash.getViewView());
    }

    private void initView(AdvancedWebView view) {
        view.setListener(splash, this);
        view.setCookiesEnabled(true);
        view.setThirdPartyCookiesEnabled(true);
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setDomStorageEnabled(true);
        view.getSettings().setDatabaseEnabled(true);
    }

    public void show(String url) {
        splash.getViewView().loadUrl(url);
    }

    public void openFile(int requestCode, int resultCode, Intent intent) {
        splash.onActivityResult(requestCode, resultCode, intent);
    }

    public void back() {
        boolean isBack = splash.getViewView().onBackPressed();
        if (!isBack) return;
        splash.exit();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {
        splash.showContent();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        splash.showError();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
