package com.iteatgame.aoo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.security.SecureRandom;
import java.util.Locale;

public class App {

    public static final int w = 720;
    public static final int h = 1280;


    public static double distance(Vector2 obj1, Vector2 obj2) {
        return Math.sqrt(Math.pow((obj2.x - obj1.x), 2) + Math.pow((obj2.y - obj1.y), 2));
    }

    public static float randF(float min, float max){
        float random = min + new SecureRandom().nextFloat() * (max - min);
        return random;

    }

    public static int rand(float min, float max) {
        return new SecureRandom().nextInt((int) (max - min+1)) + (int) min;

    }

    public static void randPos(Sprite sprite) {
        int kf = rand(1, 8);
        float x,y;
        int w2 = App.w / 2;
        int h2 = App.h / 2;

        switch (kf) {
            default:
                x = rand(0 - sprite.getWidth(), w2 + sprite.getWidth());
                y = 0 - sprite.getHeight();
                break;
            case 2:
                x = rand(w2 - sprite.getWidth(), App.w + sprite.getWidth());
                y = 0 - sprite.getHeight();
                break;
            case 3:
                x = App.w;
                y = rand(0 - sprite.getHeight(), h2 + sprite.getHeight());
                break;
            case 4:
                x = App.w;
                y = rand(h2 - sprite.getHeight(), App.h + sprite.getHeight());
                break;
            case 5:
                x = rand(w2 - sprite.getWidth(), App.w + sprite.getWidth());
                y = App.h;
                break;
            case 6:
                x = rand(0 - sprite.getWidth(), w2 + sprite.getWidth());
                y = App.h;
                break;
            case 7:
                x = 0-sprite.getWidth();
                y = rand(h2 - sprite.getHeight(), App.h + sprite.getHeight());
                break;
            case 8:
                x = 0-sprite.getWidth();
                y = rand(0 - sprite.getHeight(), h2 + sprite.getHeight());
                break;


        }

        sprite.setPosition(x, y);

    }

    public static Sprite randSprite(){
        int kf = rand(1,5);
        return new Sprite(new Texture(Gdx.files.internal("img/food"+kf+".png")));
    }

    private static String lang(){
        String lang = "";
        if(Locale.getDefault().getLanguage() != null) lang = Locale.getDefault().getLanguage();
        return lang;
    }

    /*ПЕРЕВОДЫ НИЖЕ*/

    public static String TXT_GAME_INFO(){
        return lang().equalsIgnoreCase("ru")?"Вес не главное!\nПросто героиня нашей игры хочет похудеть": "Weight is not important!\nIt's just that the heroine of our game wants to lose weight";
    }

    public static String TXT_GAME_START(){
        return lang().equalsIgnoreCase("ru")?"НАЧАТЬ ИГРУ":"START GAME";
    }

    public static String TXT_GAME_POLICY(){
        return lang().equalsIgnoreCase("ru")?"НАША ПОЛИТИКА":"POLICY";
    }

    public static String TXT_GAME_MUSIC(){
        Preferences prefs = Gdx.app.getPreferences("GAME");
        String var1 = lang().equalsIgnoreCase("ru")?"МУЗЫКА":"MUSIC";
        return var1+": "+TXT_GAME_MUSIC_VAL(prefs.getBoolean("music",true));
    }

    public static String TXT_GAME_MUSIC_VAL(boolean on){
        if(on){
            return lang().equalsIgnoreCase("ru")?"ВКЛ":"ON";
        }
        return lang().equalsIgnoreCase("ru")?"ВЫКЛ":"OFF";
    }

    public static String TXT_END_GAME(){
        return lang().equalsIgnoreCase("ru")?"Игра окончена!":"game over!";
    }
}
