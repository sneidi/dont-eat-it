package com.iteatgame.aoo.model;

import com.badlogic.gdx.Gdx;
import com.iteatgame.aoo.App;

public class Level {
    private float speed;
    private float max;
    private int score;
    private boolean demo;

    public Level(boolean demo) {
        this.demo = demo;
        update();
    }

    public Level() {
        update();
    }

    public void score(int score) {
        this.score = score;
        update();
    }

    public float getSpeed() {
        return speed;
    }

    public float getMax() {
        return max;
    }

    private void update(){
        if(demo){
            speed = App.randF(2.1f,3.1f);
            max = 1;
        } else
        if(score>199){
            speed =  App.randF(2.0f,4.0f);
            max = 8;
        } else if(score>149){
            speed =  App.randF(1.5f,2.0f);
            max = 7;

        } else if(score>99){
            speed =  App.randF(1.0f,1.5f);
            max = 6;

        } else if(score>49){
            speed =  App.randF(0.7f,1.0f);
            max = 5;

        } else if(score>19){
            speed =  App.randF(0.7f,0.9f);
            max = 4;

        } else if(score>9){
            speed = App.randF(0.6f,0.8f);
            max = 3;

        } else if(score>1){
            speed = App.randF(0.6f,0.7f);
            max = 3;

        } else {
            speed = App.randF(0.5f,0.6f);
            max = 2;
        }
    }
}
