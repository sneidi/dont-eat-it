package com.iteatgame.aoo.model;

public class ASSETS {
    public static final String CHARS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.:;,{}\"´`'<>";

    public static final String BACKGROUND="img/bg.png";
    public static final String BACKGROUND_DEMO="img/demo.png";

    public static final String ME="img/me.png";
    public static final String ME2="img/me2.png";

    public static final String REGULAR="font/regular.ttf";
    public static final String BOLD="font/bold.ttf";

    public static final String MUSIC_BG="sound/bg.mp3";
    public static final String SOUND0="sound/0.wav";
    public static final String SOUND1="sound/1.wav";

    /*ПЕРЕВОДЫ НИЖЕ*/

}
