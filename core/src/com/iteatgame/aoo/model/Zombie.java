package com.iteatgame.aoo.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.iteatgame.aoo.App;
import com.iteatgame.aoo.manager.ZombieListener;


public class Zombie {

    private Sprite sprite;
    private float speed;
    private ZombieListener listener;

    public Zombie(float speed, ZombieListener listener) {
        this.speed = speed;
        this.sprite = App.randSprite();
        this.listener = listener;
        App.randPos(sprite);
    }


    public void setListener(ZombieListener listener) {
        this.listener = listener;
    }

    public void comeback(float speed){
        dispose();
        this.speed = speed;
        this.sprite = App.randSprite();
        App.randPos(sprite);
        Gdx.app.log("GAME","new x:"+sprite.getX()+", y:"+sprite.getY()+", speed"+speed);
    }

    private void update() {

        float posX = sprite.getX();
        float posY = sprite.getY();
        float goalX = (App.w-sprite.getWidth())/2;
        float goalY = (App.h-sprite.getHeight())/2;

        float destX = goalX - posX;
        float destY = goalY - posY;

        float dist = (float) Math.sqrt(destX * destX + destY * destY);
        destX = destX / dist;
        destY = destY / dist;

        float travelX = destX * speed;
        float travelY = destY * speed;

        float distTravel = (float) Math.sqrt(travelX * travelX + travelY * travelY);

        if (distTravel > dist) {
            comeback(speed);
            if(listener!=null) {
                listener.end();
            }
        } else {
            posX += travelX;
            posY += travelY;
            sprite.setPosition(posX, posY);
        }
    }

    public void render(SpriteBatch batch) {
        update();
        sprite.draw(batch);
    }

    public Vector2 getPos() {
        return new Vector2(sprite.getX(), sprite.getY());
    }

    public void dispose() {
        sprite.getTexture().dispose();
    }


    public boolean isAttack(float touchX, float touchY) {
        return (touchX >= sprite.getX()) && touchX <= (sprite.getX() + sprite.getWidth()) && (touchY >= sprite.getY()) && touchY <= (sprite.getY() + sprite.getHeight());
    }

}