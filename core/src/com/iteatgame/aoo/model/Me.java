package com.iteatgame.aoo.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.iteatgame.aoo.App;

import static com.iteatgame.aoo.model.ASSETS.ME;
import static com.iteatgame.aoo.model.ASSETS.ME2;

import java.util.Random;


public class Me {
    private Sprite sprite, sprite2;
    private int score;
    private boolean fat;


    public int getScore() {
        return score;
    }

    public void score() {
        this.score+=1;
    }

    public void setFat(boolean fat) {
        this.fat = fat;
    }

    public Me() {
        fat = false;
        sprite = new Sprite(new Texture(Gdx.files.internal(ME)));
        sprite.setPosition((App.w-sprite.getWidth())/2, (App.h-sprite.getHeight())/2);
        sprite2 = new Sprite(new Texture(Gdx.files.internal(ME2)));
        sprite2.setPosition((App.w-sprite.getWidth())/2, (App.h-sprite.getHeight())/2);
    }

    public void render(SpriteBatch batch) {
        if(fat)sprite2.draw(batch); else sprite.draw(batch);
    }

    public void dispose() {
        sprite.getTexture().dispose();
        sprite2.getTexture().dispose();
    }

    public boolean isAttack(float touchX, float touchY) {
        return (touchX >= sprite.getX()) && touchX <= (sprite.getX() + sprite.getWidth()) && (touchY >= sprite.getY()) && touchY <= (sprite.getY() + sprite.getHeight());
    }

}
