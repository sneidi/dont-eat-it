package com.iteatgame.aoo;

import com.gamedecst.AndroidInterface;

public class MyGame extends com.badlogic.gdx.Game {
	private AndroidInterface aos;

	public MyGame(AndroidInterface aos) {
		this.aos = aos;
	}

	public AndroidInterface getAos() {
		return aos;
	}

	@Override
	public void create() {
		this.setScreen(new MyScreen(this));
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}
