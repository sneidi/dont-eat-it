package com.iteatgame.aoo.manager.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.iteatgame.aoo.manager.MyManager;
import com.iteatgame.aoo.manager.ScoreListener;
import com.iteatgame.aoo.manager.TouchListener;
import com.iteatgame.aoo.manager.sub.SubInput;
import com.iteatgame.aoo.manager.sub.SubModel;
import com.iteatgame.aoo.manager.sub.SubSound;
import com.iteatgame.aoo.manager.sub.SubUi;

public class DemoView extends ViewView implements TouchListener, ScoreListener {


    public DemoView(MyManager manager) {
        super(manager);
        subSound = new SubSound(true,Gdx.app.getPreferences("GAME").getBoolean("music",true));
        subModel = new SubModel(true);
        subModel.setListener(this);

        subInput.setListener(this);
        subUi.setListener(manager);
    }



    @Override
    public void show() {
        subUi.showBg(batch);
        subModel.action(batch);
        subUi.demo(batch);
        subInput.action(batch);
    }

    @Override
    public void destroy() {
        subUi.dispose();
        subModel.dispose();
        subInput.dispose();
        subSound.dispose();
    }

    @Override
    public void onTouch(float x, float y) {
       subModel.touch(x,y);
       subUi.touches(x,y);
    }


    @Override
    public void onScore(int score) {
        subUi.score(score);
    }

    @Override
    public void onEnd() {
    }
}
