package com.iteatgame.aoo.manager.view;

import com.badlogic.gdx.Gdx;
import com.iteatgame.aoo.App;
import com.iteatgame.aoo.manager.MyManager;
import com.iteatgame.aoo.manager.ScoreListener;
import com.iteatgame.aoo.manager.TouchListener;
import com.iteatgame.aoo.manager.sub.SubModel;
import com.iteatgame.aoo.manager.sub.SubSound;

public class GameView  extends ViewView implements TouchListener, ScoreListener {


    public GameView(MyManager manager) {
        super(manager);
        subSound = new SubSound(false, Gdx.app.getPreferences("GAME").getBoolean("music",true));
        subModel = new SubModel(false);
        subModel.setListener(this);

        subInput.setListener(this);
        subUi.setListener(manager);
    }



    @Override
    public void show() {
        subUi.showBg(batch);
        subUi.showScore(batch);
        subModel.action(batch);
        subInput.action(batch);
    }

    @Override
    public void destroy() {
        subUi.dispose();
        subModel.dispose();
        subInput.dispose();
        subSound.dispose();
    }

    @Override
    public void onScore(int score) {
        subUi.score(score);
        subSound.playOne();

    }

    @Override
    public void onEnd() {

        subSound.playZero();
        if(manager!=null && manager.aos!=null) manager.aos.toast(App.TXT_END_GAME());
        if(manager!=null) manager.setDemo(true);

    }

    @Override
    public void onTouch(float x, float y) {
        subModel.touch(x,y);

    }
}
