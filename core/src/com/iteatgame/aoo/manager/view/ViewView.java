package com.iteatgame.aoo.manager.view;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.iteatgame.aoo.manager.MyManager;
import com.iteatgame.aoo.manager.sub.SubInput;
import com.iteatgame.aoo.manager.sub.SubModel;
import com.iteatgame.aoo.manager.sub.SubSound;
import com.iteatgame.aoo.manager.sub.SubUi;

public class ViewView {


    protected SubModel subModel;
    protected SubSound subSound;
    protected SubUi subUi;
    protected SubInput subInput;

    protected MyManager manager;
    protected SpriteBatch batch;
    protected OrthographicCamera cam;

    public ViewView(MyManager manager) {
        this.manager = manager;
        this.batch = manager.batch;
        this.cam = manager.cam;

        subModel = new SubModel(false);
        subSound = new SubSound(true,false);
        subUi = new SubUi();
        subInput = new SubInput(cam);
    }

    public void show(){

    }

    public void destroy(){


    }

    public void onPause(){

        subModel.pause();
        subSound.pause();
        subUi.pause();
        subInput.pause();
    }

    public void onResume(){

        subModel.resume();
        subSound.resume();
        subUi.resume();
        subInput.resume();
    }

}
