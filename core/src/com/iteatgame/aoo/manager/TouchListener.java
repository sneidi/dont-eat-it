package com.iteatgame.aoo.manager;

public interface TouchListener {
    void onTouch(float x, float y);
}
