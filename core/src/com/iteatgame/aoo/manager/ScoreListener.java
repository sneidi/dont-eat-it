package com.iteatgame.aoo.manager;

public interface ScoreListener {
    void onScore(int score);
    void onEnd();
}
