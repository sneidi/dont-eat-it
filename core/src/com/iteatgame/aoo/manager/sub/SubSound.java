package com.iteatgame.aoo.manager.sub;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.security.SecureRandom;

import static com.iteatgame.aoo.model.ASSETS.MUSIC_BG;
import static com.iteatgame.aoo.model.ASSETS.SOUND0;
import static com.iteatgame.aoo.model.ASSETS.SOUND1;

public class SubSound extends SubSub{
    private Music bg;
    private Sound zero, one;

    private boolean demo;
    private boolean isOn;

    public SubSound(boolean demo, boolean isOn) {
        this.demo = demo;
        this.isOn = isOn;


        bg = Gdx.audio.newMusic(Gdx.files.internal(MUSIC_BG));
        bg.setVolume(0.7f);
        bg.setLooping(true);

        zero = Gdx.audio.newSound(Gdx.files.internal(SOUND0));
        one = Gdx.audio.newSound(Gdx.files.internal(SOUND1));

        playBg();
    }

    @Override
    public void action(SpriteBatch batch) {

    }

    public void playBg(){
        if(!demo && isOn) bg.play();
    }

    public void playOne(){
        if(!demo && isOn) one.play();
    }

    public void playOneMenu(){
        one.play();
    }

    public void playZero(){
        if(bg.isPlaying()) bg.stop();
        if(!demo && isOn) zero.play();
    }

    @Override
    public void resume() {
        if(!demo && isOn) bg.play();
    }

    @Override
    public void pause() {
        bg.pause();
    }

    @Override
    public void dispose() {
        bg.dispose();

        zero.dispose();
        one.dispose();
    }
}
