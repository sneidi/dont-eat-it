package com.iteatgame.aoo.manager.sub;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.iteatgame.aoo.manager.TouchListener;

public class SubInput extends SubSub implements InputProcessor {

    private OrthographicCamera cam;
    private TouchListener listener;


    public SubInput(OrthographicCamera cam) {
        this.cam = cam;
        Gdx.input.setInputProcessor(this);
    }

    public void setListener(TouchListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 cords = cam.unproject(new Vector3(screenX,screenY,0));
        listener.onTouch(cords.x,cords.y);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    @Override
    public void action(SpriteBatch batch) {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void dispose() {

    }
}
