package com.iteatgame.aoo.manager.sub;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.iteatgame.aoo.App;
import com.iteatgame.aoo.manager.ScoreListener;
import com.iteatgame.aoo.manager.ZombieListener;
import com.iteatgame.aoo.model.Level;
import com.iteatgame.aoo.model.Zombie;
import com.iteatgame.aoo.model.Me;

public class SubModel extends SubSub implements ZombieListener {

    private Me me;
    private Array<Zombie> zombies;
    private Level level;
    private boolean demo;
    private ScoreListener listener;

    public SubModel(boolean demo) {

        this.demo = demo;
        me = new Me();
        zombies = new Array<Zombie>();
        level = new Level(demo);
    }

    public void setListener(ScoreListener listener) {
        this.listener = listener;
    }

    @Override
    public void action(SpriteBatch batch) {
        try{

            level.score(me.getScore());
            if (zombies.size < level.getMax()) zombies.add(new Zombie(level.getSpeed(),this));

            me.render(batch);
            for (Zombie zombie : zombies) zombie.render(batch);
        } catch (Exception e){

        }

    }

    public void touch(float x, float y){

        if(demo && me.isAttack(x,y)) me.setFat(false);

        if(demo) return;
        for(int i=zombies.size-1;i>=0;i--){
            Zombie zombie = zombies.get(i);
            if(zombie.isAttack(x,y)){
                me.score();
                if(listener!=null) listener.onScore(me.getScore());
                zombie.comeback(level.getSpeed());
                break;
            }
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    public void dispose() {
        me.dispose();
        for (Zombie zombie : zombies) zombie.dispose();
    }


    @Override
    public void end() {
        if(listener!=null) listener.onEnd();
        me.setFat(true);
    }
}
