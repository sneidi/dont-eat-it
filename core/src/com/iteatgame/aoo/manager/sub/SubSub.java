package com.iteatgame.aoo.manager.sub;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class SubSub {

    public abstract void action(SpriteBatch batch);

    public abstract void resume();

    public abstract void pause();

    public abstract void dispose();
}
