package com.iteatgame.aoo.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gamedecst.AndroidInterface;
import com.iteatgame.aoo.App;
import com.iteatgame.aoo.manager.view.DemoView;
import com.iteatgame.aoo.manager.view.GameView;
import com.iteatgame.aoo.manager.view.ViewView;

public class MyManager implements MenuListener {

    public AndroidInterface aos;

    public SpriteBatch batch;
    public OrthographicCamera cam;

    private ViewView view;
    private boolean demo;


    public void setDemo(boolean demo) {
        if (this.demo == demo) return;
        if (view != null) view.destroy();
        this.demo = demo;
        view = demo ? new DemoView(this) : new GameView(this);
    }

    public MyManager(AndroidInterface aos) {
        this.aos = aos;

        batch = new SpriteBatch();
        cam = new OrthographicCamera();

        cam.setToOrtho(false, App.w,App.h);
        demo = true;
        view = new DemoView(this);
    }

    public void show() {
        cam.update();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        if(view!=null) view.show();
        batch.end();
    }

    public void resume() {
        view.onResume();
    }

    public void pause() {
        view.onPause();
    }

    public void stop() {
        view.destroy();
    }


    @Override
    public void startGame() {
        setDemo(false);
    }

    @Override
    public void policy() {
        if(aos!=null) aos.openPolicy();
    }

    @Override
    public void music() {
        Preferences prefs = Gdx.app.getPreferences("GAME");
        boolean music = prefs.getBoolean("music",true);
        prefs.putBoolean("music",!music);
        prefs.flush();
    }
}
