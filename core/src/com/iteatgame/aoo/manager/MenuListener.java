package com.iteatgame.aoo.manager;

public interface MenuListener {
    void startGame();
    void policy();
    void music();
}
