package com.iteatgame.aoo;

import com.badlogic.gdx.Screen;
import com.iteatgame.aoo.manager.MyManager;

public class MyScreen implements Screen {
    private MyGame game;
    private MyManager manager;

    public MyScreen(MyGame game) {
        this.game = game;
        this.manager = new MyManager(game.getAos());
    }

    @Override
    public void show() {
        manager.show();
    }

    @Override
    public void resume() {
        manager.resume();
    }

    @Override
    public void pause() {
        manager.pause();
    }

    @Override
    public void dispose() {
        manager.stop();
    }

    @Override
    public void render(float delta) {
        manager.show();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void hide() {

    }
}