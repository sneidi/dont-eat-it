package com.iteatgame.aoo;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Desktop {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 432;
		config.height = 720;
		config.resizable = false;

		new LwjglApplication(new MyGame(null), config);
	}
}
